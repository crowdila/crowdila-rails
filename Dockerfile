FROM ruby:2.4

RUN gem install bundler
RUN apt update -y && apt install -y nodejs

RUN mkdir /var/app
WORKDIR /var/app

ADD . .

RUN bundle install

CMD ["rails", "s", "-p3000"]

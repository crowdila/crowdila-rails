class Api::Internal::AnswerController < ApplicationController
  def show
    answers = params.require(:ids).map { |id| Answer.find_by(id: id).as_json }
    render json: answers
  end

  def create
    post_id = params.require(:post_id)
    user_id = params.require(:user_id)
    answer = Answer.create(post_id: post_id, user_id: user_id)
    question_answers = params.require(:question_answers)
    question_answers.each do |qa|
      if qa[:option_ids]
        qa[:option_ids].each do |opt|
          QuestionAnswer.create!({ question_id: qa.require(:question_id) }.merge(option_id: opt, answer: answer))
        end
      else
        QuestionAnswer.create!({ text: qa.require(:text), question_id: qa.require(:question_id) }.merge(answer: answer))
      end
    end
    answer = answer.reload.as_json
    render json: answer
  end
end

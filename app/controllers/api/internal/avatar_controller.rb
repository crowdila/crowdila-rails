class Api::Internal::AvatarController < ApplicationController
  def show
    avatars = params.require(:ids).map { |id| Avatar.find_by(id: id).as_json }
    render json: avatars
  end

  def index
    avatars = Avatar.all.map(&:as_json)
    render json: avatars
  end
end

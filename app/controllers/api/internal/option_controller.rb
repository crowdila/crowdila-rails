class Api::Internal::OptionController < ApplicationController
  def show
    options = params.require(:ids).map { |id| Option.find_by(id: id).as_json }
    render json: options
  end

  def create
    option = Option.create(question_id: params.require(:question_id), text: params.require(:option)).as_json
    render json: option
  end

  def options_by_question_id
    options = Option.where(question_id: params.require(:question_id)).map(&:as_json)
    render json: options
  end
end

class Api::Internal::PostController < ApplicationController
  def show
    posts = params.require(:ids).map { |id| Post.find_by(id: id).as_json }
    render json: posts
  end

  def posts_by_title
    posts = Post.where('title like ?', "%#{params.require(:title)}%")
                .where('id > ?', params.require(:id_from))
                .limit(params.require(:limit))
                .map(&:as_json)
    render json: posts
  end

  def posts_by_tag_names
    posts = Post.joins(:tags)
                .where(tags: { name: (params.permit(tag_names: [])[:tag_names] || []).to_a })
                .where('posts.id > ?', params.require(:id_from))
                .limit(params.require(:limit))
                .map(&:as_json)
    render json: posts
  end

  def posts_by_user_id
    posts =  Post.where({ user_id: params.require(:user_id) })
                 .where('id > ?', params.require(:id_from))
                 .limit(params.require(:limit))
                 .map(&:as_json)
    render json: posts
  end

  def create
    title = params.require(:title)
    tags = params.require(:tag_names)
    user_id = params.require(:user_id)
    questions = params.require(:questions)
    return render json: { message: 'Questions count can\'t be zero' }, status: 400 if questions.count < 1
    post = ActiveRecord::Base.transaction do
      post = Post.create!(user_id: user_id, title: title, questions_count: questions.count)
      tags.each do |tag_name|
        tag = Tag.find_by(name: tag)
        next unless tag
        PostTag.create!(post: post, tag: tag)
      end
      questions.each do |q|
        question = Question.create!(q.permit(:text, :type, :multi_answers, :user_can_add).merge({ post_id: post.id }))
        if q.require(:type) == 'McqQuestion'
          opts = q.require(:options)
          return render json: { message: 'Option count can\'t be < 2' }, status: 400 if opts.count < 2
          opts.each do |opt|
            question.options.create!(text: opt)
          end
        end
      end
      post
    end
    render json: post.as_json
  end
end

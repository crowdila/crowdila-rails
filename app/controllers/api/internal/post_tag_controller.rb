class Api::Internal::PostTagController < ApplicationController
  def show
    post_tags = params.require(:ids).map { |id| PostTag.find_by(id: id).as_json }
    render json: post_tags
  end
end

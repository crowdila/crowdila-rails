class Api::Internal::QuestionAnswerController < ApplicationController
  def show
    question_answers = params.require(:ids).map { |id| QuestionAnswer.find_by(id: id).as_json }
    render json: question_answers
  end
end

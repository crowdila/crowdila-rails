class Api::Internal::QuestionController < ApplicationController
  def show
    questions = params.require(:ids).map { |id| Question.find_by(id: id).decorate }
    render json: questions
  end

  def questions_by_post_id
    questions = Question.where(post_id: params.require(:post_id)).map(&:decorate)
    render json: questions
  end
end

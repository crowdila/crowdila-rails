class Api::Internal::TagController < ApplicationController
  def index
    tags = Tag.all.map(&:as_json)
    render json: tags
  end

  def show
    tags = params.require(:ids).map { |id| Tag.find_by(id: id).as_json }
    render json: tags
  end

  def tags_by_user_id
    tags = Tag.joins(:users)
              .where(users: { id: params.require(:user_id) })
              .map(&:as_json)
    render json: tags
  end

  def tags_by_post_id
    tags = Tag.joins(:posts)
              .where(posts: { id: params.require(:post_id) })
              .map(&:as_json)
    render json: tags
  end
end

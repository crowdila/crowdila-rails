class Api::Internal::UserController < ApplicationController
  after_action :update_last_activity, only: [:update_tags, :update_avatar]

  def show
    users = params.require(:ids).map { |id| User.find_by(id: id).as_json }
    render json: users
  end

  def user_by_username_and_password
    user = User.find_by!(username: params.require(:username)).authenticate(params.require(:password)).as_json
    render json: user
  end

  def user_by_username_and_token
    user = User.find_by!(username: params.require(:username), token: params.require(:token)).as_json
    render json: user
  end

  def create
    data = { username: params.require(:username), email: params.require(:email), password: params.require(:password) }.merge(avatar_id: Avatar.all.pluck(:id).sample)
    user = User.create(data).as_json
    render json: user
  end

  def update_tags
    UserTag.where(user_id: params.require(:id)).destroy_all
    tag_ids = Tag.where(name: params.require(:tag_names)).pluck(:id)
    tag_ids.each do |tag_id|
      UserTag.create(user_id: params.require(:id), tag_id: tag_id)
    end
    user = User.find_by!(id: params.require(:id)).as_json
    render json: user
  end

  def update_avatar
    User.update(params.require(:id), avatar_id: params.require(:avatar_id))
    user = User.find_by!(id: params.require(:id)).as_json
    render json: user
  end

  def update_last_activity
    User.update(params.require(:id), last_activity: Time.now)
  end
end

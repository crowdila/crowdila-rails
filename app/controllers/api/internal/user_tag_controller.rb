class Api::Internal::UserTagController < ApplicationController
  def show
    user_tags = params.require(:ids).map { |id| UserTag.find_by(id: id).as_json }
    render json: user_tags
  end
end

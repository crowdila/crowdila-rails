class ApplicationController < ActionController::Base
  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.
  protect_from_forgery with: :null_session

  rescue_from ActionController::ParameterMissing do |e|
    render json: {message: e.message}, status: 400
  end

  rescue_from ActiveRecord::RecordNotFound do |e|
    render json: {message: e.message}, status: 404
  end

  rescue_from ActiveRecord::RecordNotSaved, ActiveRecord::RecordInvalid do |e|
    render json: {message: e.record.errors.full_messages.join(', ')}, status: 422
  end
end

class Answer < ActiveRecord::Base
  # associations
  has_many :question_answers
  belongs_to :user
  belongs_to :post

  # validations
  validates :user, presence: true
  validates :post, presence: true

  # callbacks
  before_save :increase_post_count, on: :create

  def increase_post_count
    post.answer_count = 0 unless post.answer_count
    post.answer_count += 1
    post.save
  end
end

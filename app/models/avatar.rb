class Avatar < ActiveRecord::Base
  # validations
  validates :avatar_url, presence: true
end

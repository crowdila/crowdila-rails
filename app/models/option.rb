class Option < ActiveRecord::Base
  # associations
  belongs_to :question
  has_many :question_answers

  # validations
  validates :question, presence: true
  validates :text, presence: true, length: { maximum: 140 }
end

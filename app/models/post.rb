class Post < ActiveRecord::Base
  # associations
  belongs_to :user
  has_many :questions
  has_many :post_tags
  has_many :tags, through: :post_tags

  # validations
  validates :user, presence: true
  validates :title, presence: true, length: { maximum: 140 }
end

class PostTag < ActiveRecord::Base
  # associations
  belongs_to :post
  belongs_to :tag

  # validations
  validates :post, presence: true
  validates :tag, presence: true
end

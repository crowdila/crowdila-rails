class Question < ActiveRecord::Base
  # associations
  belongs_to :post
  has_many :options
  has_many :question_answers

  # validations
  validates :post, presence: true
  validates :text, presence: true, length: { maximum: 140 }
  validates :type, presence: true

  def decorate
    as_json.merge(type: type)
  end
end

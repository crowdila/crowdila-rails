class QuestionAnswer < ActiveRecord::Base
  # associations
  belongs_to :question
  belongs_to :answer
  belongs_to :option

  # validations
  validates :question, presence: true
  validates :answer, presence: true
  validates :option, presence: true, if: -> { question.type == 'McqQuestion' }
  validates :text, presence: true, length: { maximum: 140 }, if: -> { question.type == 'TextQuestion' }

  # callbacks
  before_save :increase_option_count, on: :create, if: -> { question.type == 'McqQuestion' }

  def increase_option_count
    option.answer_count = 0 unless option.answer_count
    option.answer_count += 1
    option.save
  end
end

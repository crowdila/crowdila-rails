class Tag < ActiveRecord::Base
  # associations
  has_many :post_tags
  has_many :posts, through: :post_tags
  has_many :user_tags
  has_many :users, through: :user_tags

  # validations
  validates :name, presence: true, length: { maximum: 20 }, uniqueness: true
end

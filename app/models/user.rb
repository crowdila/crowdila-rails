class User < ActiveRecord::Base
  has_secure_password

  # associations
  belongs_to :avatar
  has_many :posts
  has_many :user_tags
  has_many :tags, through: :user_tags

  # validations
  validates :username, presence: true, uniqueness: true, length: { in: 3..20 }
  validates :avatar, presence: true
  validates :email, presence: true, uniqueness: true

  # callbacks
  before_save :assign_token, on: :create
  before_save :assign_last_activity, on: :create

  def assign_last_activity
    self.last_activity = Time.now
  end

  def assign_token
    self.token ||= SecureRandom.base64(24)
  end
end

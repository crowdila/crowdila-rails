class UserTag < ActiveRecord::Base
  # associations
  belongs_to :user
  belongs_to :tag

  # validations
  validates :user, presence: true
  validates :tag, presence: true
end

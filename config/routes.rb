Rails.application.routes.draw do
  namespace :api do
    namespace :internal do
      resources :answer, only: [] do
        collection do
          get '/find_by_ids', to: 'answer#show'
          post '/', to: 'answer#create'
        end
      end

      resources :avatar, only: [] do
        collection do
          get '/', to: 'avatar#index'
          get '/find_by_ids', to: 'avatar#show'
        end
      end

      resources :option, only: [] do
        collection do
          get '/find_by_ids', to: 'option#show'
          post '/', to: 'option#create'
          get '/find_by_question_id', to: 'option#options_by_question_id'
        end
      end

      resources :post, only: [] do
        collection do
          get '/find_by_ids', to: 'post#show'
          get '/find_by_title', to: 'post#posts_by_title'
          get '/find_by_tags', to: 'post#posts_by_tag_names'
          get '/find_by_user_id', to: 'post#posts_by_user_id'
          post '/', to: 'post#create'
        end
      end

      resources :post_tag, only: [] do
        collection do
          get '/find_by_ids', to: 'post_tag#show'
        end
      end

      resources :question, only: [] do
        collection do
          get '/find_by_ids', to: 'question#show'
          get 'find_by_post_id', to: 'question#questions_by_post_id'
        end
      end

      resources :question_answer, only: [] do
        collection do
          get '/find_by_ids', to: 'question_answer#show'
        end
      end

      resources :tag, only: [] do
        collection do
          get '/', to: 'tag#index'
          get '/find_by_ids', to: 'tag#show'
          get '/find_by_user_id', to: 'tag#tags_by_user_id'
          get '/find_by_post_id', to: 'tag#tags_by_post_id'
        end
      end

      resources :user, only: [] do
        collection do
          get '/find_by_ids', to: 'user#show'
          get '/login', to: 'user#user_by_username_and_password'
          get '/auth', to: 'user#user_by_username_and_token'
          post '/', to: 'user#create'
          post '/update_tags', to: 'user#update_tags'
          post '/update_avatar', to: 'user#update_avatar'
        end
      end

      resources :user_tag, only: [] do
        collection do
          get '/find_by_ids', to: 'user_tag#show'
        end
      end
    end
  end
end

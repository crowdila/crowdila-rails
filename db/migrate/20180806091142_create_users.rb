class CreateUsers < ActiveRecord::Migration
  def change
    create_table :users do |t|
      t.string :username
      t.string :email
      t.string :password_digest
      t.datetime :last_activity
      t.string :token
      t.references :avatar

      t.timestamps null: false
    end
  end
end

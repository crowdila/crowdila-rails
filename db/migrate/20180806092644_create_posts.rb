class CreatePosts < ActiveRecord::Migration
  def change
    create_table :posts do |t|
      t.string :title
      t.integer :answer_count
      t.string :uuid
      t.references :user

      t.timestamps null: false
    end
  end
end

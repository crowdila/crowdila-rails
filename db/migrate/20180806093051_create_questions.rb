class CreateQuestions < ActiveRecord::Migration
  def change
    create_table :questions do |t|
      t.string :text
      t.string :type
      t.boolean :multi_answers
      t.boolean :user_can_add
      t.references :post

      t.timestamps null: false
    end
  end
end

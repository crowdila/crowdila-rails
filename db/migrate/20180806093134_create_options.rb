class CreateOptions < ActiveRecord::Migration
  def change
    create_table :options do |t|
      t.string :text
      t.integer :answer_count
      t.string :uuid
      t.references :question

      t.timestamps null: false
    end
  end
end

class CreateQuestionAnswers < ActiveRecord::Migration
  def change
    create_table :question_answers do |t|
      t.string :text
      t.references :answer
      t.references :question
      t.references :option

      t.timestamps null: false
    end
  end
end

class AddQuestionsCountToPost < ActiveRecord::Migration[5.0]
  def change
    add_column :posts, :questions_count, :integer
  end
end

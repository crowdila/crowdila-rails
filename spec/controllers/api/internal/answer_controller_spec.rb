require 'rails_helper'

RSpec.describe Api::Internal::AnswerController, type: :request do
  let(:avatar) { Avatar.create(avatar_url: 'junkie') }
  let(:email) { 'junk@rat.ow' }
  let(:username) { 'junkrat' }
  let(:password) { 'eyy_lmao' }
  let!(:user) { User.create(username: username, avatar: avatar, email: email, password: password) }

  let(:post_title) { 'what a wonderful lil post' }
  let!(:post1) { Post.create(user: user, title: post_title) }

  let(:question_text) { 'this is a question' }
  let!(:mcq_question) { Question.create(post: post1, type: 'McqQuestion', text: question_text + '1') }
  let!(:text_question) { Question.create(post: post1, type: 'TextQuestion', text: question_text + '2') }

  let(:option_text) { 'is this real life?' }
  let!(:option) { Option.create(question: mcq_question, text: option_text) }


  describe 'queries' do
    describe '#show' do

      let(:url) { '/api/internal/answer/find_by_ids' }
      let!(:answer) { Answer.create(post: post1, user: user) }
      let!(:text_question_answer) {  QuestionAnswer.create(question: text_question, text: 'eyyyyyyyyy', answer: answer) }
      let!(:mcq_question_answer) { QuestionAnswer.create(question: mcq_question, option: option, answer: answer) }

      context 'when params are correct' do
        let(:request_params) { { ids: [answer.id] } }

        it 'should succeed' do
          get url, params: request_params
          expect(response.code).to eq '200'
        end
      end
    end
  end

  describe 'mutations' do
    describe '#create' do

      let(:url) { '/api/internal/answer/' }
      fcontext 'when params are correct' do
        let(:text_question_answer) { { question_id: text_question.id, text: 'lmaaaao' } }
        let(:mcq_question_answer) { { question_id: mcq_question.id, option_ids: [option.id] } }
        let!(:question_answers) { [text_question_answer, mcq_question_answer] }
        let(:request_params) { { post_id: post1.id, user_id: user.id, question_answers: question_answers } }
        let!(:old_post_count) { post1.answer_count || 0 }
        let!(:old_option_count) { option.answer_count || 0 }

        it 'should succeed and increase option and post counts' do
          post url, params: request_params
          expect(response.code).to eq '200'
          expect(Answer.find_by(user_id: user.id, post_id: post1.id)).not_to be_nil
          expect(post1.reload.answer_count).to eq old_post_count + 1
          expect(option.reload.answer_count).to eq old_option_count + 1
        end
      end
    end
  end
end

require 'rails_helper'

RSpec.describe Api::Internal::AvatarController, type: :request do
  describe 'queries' do
    let!(:avatar){ Avatar.create(avatar_url: 'abdo\'s_face') }

    describe '#show' do
      let(:url) { '/api/internal/avatar/find_by_ids' }

      context 'when params are correct' do
        let(:request_params) { { ids: [avatar.id] } }

        it 'should succeed' do
          get url, params: request_params
          expect(response.code).to eq '200'
        end
      end
    end

    describe '#index' do
      let(:url) { '/api/internal/avatar/' }

      context 'when params are correct' do

        it 'should succeed' do
          get url
          expect(response.code).to eq '200'
        end
      end
    end
  end
end

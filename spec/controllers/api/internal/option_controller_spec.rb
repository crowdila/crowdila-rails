require 'rails_helper'

RSpec.describe Api::Internal::OptionController, type: :request do
  let(:avatar) { Avatar.create(avatar_url: 'junkie') }
  let(:email) { 'junk@rat.ow' }
  let(:username) { 'junkrat' }
  let(:password) { 'eyy_lmao' }
  let!(:user) { User.create(username: username, avatar: avatar, email: email, password: password) }

  let(:post_title) { 'what a pity!' }
  let!(:post1) { Post.create(user: user, title: post_title) }

  let(:question_text) { 'don\'t small us' }
  let!(:question) { Question.create(post: post1, text: question_text, type: 'TextQuestion') }

  let(:option_text) { 'some random option text' }
  let!(:option1) { Option.create(question: question, text: option_text + '1') }
  let!(:option2) { Option.create(question: question, text: option_text + '2') }

  describe 'queries' do
    describe '#show' do
      let(:url) { '/api/internal/option/find_by_ids' }

      context 'when params are correct' do
        let(:request_params) { { ids: [option1.id, option2.id] } }

        it 'should succeed' do
          get url, params: request_params
          expect(response.code).to eq '200'
          expect(JSON.parse(response.body).count).to eq 2
        end
      end
    end

    describe '#options_by_question_id' do
      let(:url) { '/api/internal/option/find_by_question_id' }

      context 'when params are correct' do
        let(:request_params) { { question_id: question.id } }

        it 'should succeed' do
          get url, params: request_params
          expect(response.code).to eq '200'
          expect(JSON.parse(response.body).count).to eq 2
        end
      end
    end
  end

  describe 'mutations' do
    describe '#create' do
      let(:url) { '/api/internal/option/' }

      context 'when params are correct' do
        let(:request_params) { { question_id: question.id, option: option_text + '3' } }

        it 'should succeed' do
          post url, params: request_params
          expect(response.code).to eq '200'
          expect(Option.find_by(question: question.id, text: option_text + '3')).not_to be_nil
        end
      end
    end
  end
end

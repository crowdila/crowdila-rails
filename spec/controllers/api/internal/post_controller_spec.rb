require 'rails_helper'

RSpec.describe Api::Internal::PostController, type: :request do
  let!(:avatar) { Avatar.create(avatar_url: 'junkie') }
  let!(:email) { 'junk@rat.ow' }
  let!(:username) { 'junkrat' }
  let!(:password) { 'eyy_lmao' }
  let!(:title) { 'does a submarine swim?' }
  let!(:tag1) { Tag.create(name: 'overwatch') }
  let!(:tag2) { Tag.create(name: 'talon') }
  let!(:user) { User.create(username: username, avatar: avatar, email: email, password: password) }

  describe 'queries' do
    let!(:post) { Post.create(user: user, title: title) }

    describe '#show' do
      let(:url) { '/api/internal/post/find_by_ids' }

      context 'when params are correct' do
        let(:request_params) { { ids: [post.id] } }

        it 'should succeed' do
          get url, params: request_params
          expect(response.code).to eq '200'
        end
      end
    end

    describe '#posts_by_title' do
      let(:url) { '/api/internal/post/find_by_title' }

      context 'when params are correct' do
        let(:request_params) { { title: title, id_from: 0, limit: 10 } }

        it 'should succeed' do
          get url, params: request_params
          expect(response.code).to eq '200'
          expect(JSON.parse(response.body)[0]['id']).to eq post.id
        end
      end
    end

    describe '#posts_by_tag_names' do
      let(:url) { '/api/internal/post/find_by_tags' }
      let!(:post_tag1) { PostTag.create(tag: tag1, post: post) }
      let!(:post_tag2) { PostTag.create(tag: tag2, post: post) }

      context 'when params are correct' do
        let(:request_params) { { tag_names: [tag1.name, tag2.name], limit: 10, id_from: 0} }

        it 'should succeed' do
          get url, params: request_params
          expect(response.code).to eq '200'
          expect(JSON.parse(response.body)[0]['id']).to eq post.id
        end
      end
    end

    describe '#posts_by_user_id' do
      let(:url) { '/api/internal/post/find_by_user_id' }

      context 'when params are correct' do
        let(:request_params) { { user_id: user.id, id_from: 0, limit: 10 } }

        it 'should succeed' do
          get url, params: request_params
          expect(response.code).to eq '200'
          expect(JSON.parse(response.body)[0]['id']).to eq post.id
        end
      end
    end
  end

  describe 'mutations' do
    describe '#create' do
      let(:url) { '/api/internal/post/' }
      let(:question) { [{text: 'eyy', type: 'McqQuestion', multi_answers: false, user_can_add: false, options: ['lmao', 'waddup']}] }
      context 'when params are correct' do
        let(:request_params) { { title: title, tag_names: [tag1.name, tag2.name], user_id: user.id, questions: question } }

        it 'should succeed' do
          post url, params: request_params
          expect(response.code).to eq '200'
          expect(Post.find_by(title: title, user: user)).not_to be_nil
        end
      end
    end
  end
end

require 'rails_helper'

RSpec.describe Api::Internal::QuestionAnswerController, type: :request do
  let(:avatar) { Avatar.create(avatar_url: 'junkie') }
  let(:email) { 'junk@rat.ow' }
  let(:username) { 'junkrat' }
  let(:password) { 'eyy_lmao' }
  let!(:user) { User.create(username: username, avatar: avatar, email: email, password: password) }

  let(:post_title) { 'what a wonderful lil post' }
  let!(:post1) { Post.create(user: user, title: post_title) }

  let(:question_text) { 'this is a question' }
  let!(:mcq_question) { Question.create(post: post1, type: 'McqQuestion', text: question_text + '1') }
  let!(:text_question) { Question.create(post: post1, type: 'TextQuestion', text: question_text + '2') }

  let(:option_text) { 'is this real life?' }
  let!(:option) { Option.create(question: mcq_question, text: option_text) }

  let!(:answer) { Answer.create(post: post1, user: user) }

  let!(:text_question_answer) {  QuestionAnswer.create(question: text_question, text: 'eyyyyyyyyy', answer: answer) }
  let!(:mcq_question_answer) { QuestionAnswer.create(question: mcq_question, option: option, answer: answer) }

  describe 'queries' do
    describe '#show' do
      let(:url) { '/api/internal/question_answer/find_by_ids' }

      context 'when params are correct' do
        let(:request_params) { { ids: [text_question_answer.id, mcq_question_answer.id] } }

        it 'should succeed' do
          get url, params: request_params
          expect(response.code).to eq '200'
          expect(JSON.parse(response.body).count).to eq 2
          expect(JSON.parse(response.body)[0]['id']).to eq text_question_answer.id
          expect(JSON.parse(response.body)[1]['id']).to eq mcq_question_answer.id
        end
      end
    end
  end
end

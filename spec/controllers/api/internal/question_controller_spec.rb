require 'rails_helper'

RSpec.describe Api::Internal::QuestionController, type: :request do
  describe 'queries' do
    let!(:avatar) { Avatar.create(avatar_url: 'junkie') }
    let!(:email) { 'junk@rat.ow' }
    let!(:username) { 'junkrat' }
    let!(:password) { 'eyy_lmao' }
    let!(:title) { 'what\'s an aimbot?' }
    let!(:user) { User.create(username: username, avatar: avatar, email: email, password: password) }
    let!(:post) { Post.create(user: user, title: title) }
    let(:text) { 'is eyy a lmao?' }
    let!(:question) { Question.create(text: text, post: post, type: 'TextQuestion') }

    describe '#show' do
      let(:url) { '/api/internal/question/find_by_ids' }

      context 'when params are correct' do
        let(:request_params) { { ids: [question.id] } }

        it 'should succeed' do
          get url, params: request_params
          expect(response.code).to eq '200'
          expect(JSON.parse(response.body).count).to eq 1
        end
      end
    end

    describe '#questions_by_post_id' do
      let(:url) { '/api/internal/question/find_by_post_id' }

      context 'when params are correct' do
        let(:request_params) { { post_id: [post.id] } }

        it 'should succeed' do
          get url, params: request_params
          expect(response.code).to eq '200'
          expect(JSON.parse(response.body).count).to eq 1
        end
      end
    end
  end
end

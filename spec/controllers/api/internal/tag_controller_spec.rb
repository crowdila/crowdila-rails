require 'rails_helper'

RSpec.describe Api::Internal::TagController, type: :request do
  describe 'queries' do
    let!(:tag1){ Tag.create(name: 'space') }
    let!(:tag2){ Tag.create(name: 'overseas') }
    let!(:avatar) { Avatar.create(avatar_url: 'junkie') }
    let!(:email) { 'junk@rat.ow' }
    let!(:username) { 'junkrat' }
    let!(:password) { 'eyy_lmao' }
    let!(:title) { 'what\'s an aimbot?' }
    let!(:user) { User.create(username: username, avatar: avatar, email: email, password: password) }
    let!(:post) { Post.create(user: user, title: title) }
    let!(:post_tag1) { PostTag.create(post: post, tag: tag1) }
    let!(:post_tag2) { PostTag.create(post: post, tag: tag2) }
    let!(:user_tag1) { UserTag.create(user: user, tag: tag1) }
    let!(:user_tag2) { UserTag.create(user: user, tag: tag2) }

    describe '#index' do
      let(:url) { '/api/internal/tag/' }

      context 'when params are correct' do

        it 'should succeed' do
          get url
          expect(response.code).to eq '200'
          expect(JSON.parse(response.body).count).to eq 2
          expect(JSON.parse(response.body)[0]['id']).to eq tag1.id
          expect(JSON.parse(response.body)[1]['id']).to eq tag2.id
        end
      end
    end

    describe '#show' do
      let(:url) { '/api/internal/tag/find_by_ids' }

      context 'when params are correct' do
        let(:request_params) { { ids: [tag1.id] } }

        it 'should succeed' do
          get url, params: request_params
          expect(response.code).to eq '200'
          expect(JSON.parse(response.body).count).to eq 1
          expect(JSON.parse(response.body)[0]['id']).to eq tag1.id
        end
      end
    end

    describe '#tags_by_user_id' do
      let(:url) { '/api/internal/tag/find_by_user_id' }

      context 'when params are correct' do
        let(:request_params) { { user_id: user.id } }

        it 'should succeed' do
          get url, params: request_params
          expect(response.code).to eq '200'
          expect(JSON.parse(response.body).count).to eq 2
        end
      end
    end

    describe '#tags_by_post_id' do
      let(:url) { '/api/internal/tag/find_by_post_id' }

      context 'when params are correct' do
        let(:request_params) { { post_id: post.id } }

        it 'should succeed' do
          get url, params: request_params
          expect(response.code).to eq '200'
          expect(JSON.parse(response.body).count).to eq 2
        end
      end
    end
  end
end

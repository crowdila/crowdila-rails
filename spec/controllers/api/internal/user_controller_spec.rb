require 'rails_helper'

RSpec.describe Api::Internal::UserController, type: :request do
  let(:avatar) { Avatar.create(avatar_url: 'junkie') }
  let(:email) { 'junk@rat.ow' }
  let(:username) { 'junkrat' }
  let(:password) { 'eyy_lmao' }
  let(:token) { 'is this easy mode?' }
  describe 'queries' do
    let!(:user) { User.create(username: username, avatar: avatar, email: email, password: password, token: token) }

    describe '#show' do
      let(:url) { '/api/internal/user/find_by_ids' }

      context 'when params are correct' do
        let(:request_params) { { ids: [user.id] } }

        it 'should succeed' do
          get url, params: request_params
          expect(response.code).to eq '200'
        end
      end

      context 'when id is not found' do
        let(:request_params) { { ids: [user.id, 0] } }

        it 'should return nil' do
          get url, params: request_params
          expect(response.code).to eq '200'
          expect(JSON.parse(response.body)[0]).not_to be_nil
          expect(JSON.parse(response.body)[1]).to be_nil
        end
      end
    end

    describe '#user_by_username_and_password' do
      let(:url) { '/api/internal/user/login' }
      let(:request_params) { { username: username, password: password } }

      it 'should succeed' do
        get url, params: request_params
        expect(response.code).to eq '200'
      end
    end

    describe '#user_by_username_and_token' do
      let(:url) { '/api/internal/user/auth' }
      let(:request_params) { { username: username, token: token } }

      it 'should succeed' do
        get url, params: request_params
        expect(response.code).to eq '200'
      end
    end
  end

  describe 'mutations' do
    describe '#create' do
      let(:url) { '/api/internal/user/' }
      let(:request_params) { { username: username, password: password, email: email, avatar_id: avatar.id } }

      it 'should succeed' do
        post url, params: request_params
        expect(response.code).to eq '200'
        expect(User.find_by(username: username)).not_to be_nil
      end
    end

    describe '#update_tags' do
      let!(:tag1) { Tag.create(name: 'overwatch') }
      let!(:tag2) { Tag.create(name: 'talon') }
      let!(:user) { User.create(username: username, avatar: avatar, email: email, password: password) }
      let!(:user_tag1) { UserTag.create(user_id: user.id, tag_id: tag1.id) }
      let!(:user_tag2) { UserTag.create(user_id: user.id, tag_id: tag2.id) }

      let(:url) { '/api/internal/user/update_tags' }
      let(:request_params) { { id: user.id, tag_names: [tag1.name] } }

      it 'should succeed' do
        expect(user.reload.tags.count).to eq 2

        post url, params: request_params
        expect(response.code).to eq '200'
        expect(user.reload.tags.count).to eq 1
      end
    end

    describe '#update_avatar' do
      let(:avatar1) { Avatar.create(avatar_url: 'roadhog') }
      let!(:user) { User.create(username: username, avatar: avatar, email: email, password: password) }

      let(:url) { '/api/internal/user/update_avatar' }
      let(:request_params) { { id: user.id, avatar_id: avatar1.id } }

      it 'should succeed' do
        expect(user.avatar_id).to eq avatar.id
        post url, params: request_params
        expect(response.code).to eq '200'
        expect(user.reload.avatar_id).to eq avatar1.id
      end
    end
  end
end

require 'rails_helper'

RSpec.describe Api::Internal::UserTagController, type: :request do
  describe 'queries' do
    let(:avatar) { Avatar.create(avatar_url: 'junkie') }
    let(:email) { 'junk@rat.ow' }
    let(:username) { 'junkrat' }
    let(:password) { 'eyy_lmao' }
    let(:token) { 'is this easy mode?' }
    let!(:user) { User.create(username: username, avatar: avatar, email: email, password: password, token: token) }
    let!(:tag) { Tag.create(name: 'overflow') }
    let!(:user_tag) { UserTag.create(user: user, tag: tag) }
    describe '#show' do
      let(:url) { '/api/internal/user_tag/find_by_ids' }

      context 'when params are correct' do
        let(:request_params) { { ids: [user_tag.id] } }

        it 'should succeed' do
          get url, params: request_params
          expect(response.code).to eq '200'
        end
      end
    end
  end
end
